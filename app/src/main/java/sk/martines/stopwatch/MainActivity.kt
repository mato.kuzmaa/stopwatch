package sk.martines.stopwatch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.widget.Chronometer
import sk.martines.stopwatch.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private var startInSeconds: Long = 0
    private var stopwatchIsRunning = false
    private var progress = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.stopwatch.format = "%s"
        binding.stopwatch.base = SystemClock.elapsedRealtime()

        binding.stopwatch.onChronometerTickListener =
            Chronometer.OnChronometerTickListener { chronometer ->
                progress =
                    (binding.stopwatch.text[3].toString() + binding.stopwatch.text[4].toString()).toFloat() * 5 / 3f
                updateProgressBar()
                if ((SystemClock.elapsedRealtime() - chronometer.base) >= 3599000) {
                chronometer.base = SystemClock.elapsedRealtime()
            }
            }
        binding.startStopwatchBtn.setOnClickListener {
            if (!stopwatchIsRunning) {
                binding.stopwatch.base = SystemClock.elapsedRealtime() - startInSeconds
                binding.stopwatch.start()
                stopwatchIsRunning = true
                binding.startStopwatchBtn.text = "Pause"
            } else {
                binding.stopwatch.stop()
                startInSeconds = SystemClock.elapsedRealtime() - binding.stopwatch.base
                stopwatchIsRunning = false
                binding.startStopwatchBtn.text = "Start"
            }
        }
        binding.resetStopwatchBtn.setOnClickListener {
            resetStopwatch()
        }
    }

    private fun resetStopwatch() {
        binding.stopwatch.stop()
        stopwatchIsRunning = false
        binding.startStopwatchBtn.text = "Start"
        binding.stopwatch.base = SystemClock.elapsedRealtime()
        startInSeconds = 0
        progress = 0f
        updateProgressBar()
    }

    private fun updateProgressBar() {
        binding.progressBar.progress = progress.toInt()
    }
}